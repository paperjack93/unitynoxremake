﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using MLAPI.Serialization;
using MLAPI.Serialization.Pooled;

public class GameManager : MonoBehaviour
{
	public static BuffList buffList;
	public BuffList _buffList;

	public static ItemList itemList;
	public ItemList _itemList;

	public static GameObject droppedItemPrefab;
	public GameObject _droppedItemPrefab;

	public virtual void Awake(){
		InitializeSerializers();
		InitializeBuffs();
		InitializeItems();
	}

	public virtual void InitializeSerializers(){
		SerializationManager.RegisterSerializationHandlers<InventoryItemData>((Stream stream, InventoryItemData instance) =>{
		    using (PooledBitWriter writer = PooledBitWriter.Get(stream)) {
		        writer.WriteInt32Packed(instance.id);
		        writer.WriteInt32Packed(instance.count);
		        writer.WriteInt32Packed(instance.index);
		        writer.WriteByte(instance.flag);
		    }
		}, (Stream stream) => {
		    using (PooledBitReader reader = PooledBitReader.Get(stream)) {
		        return new InventoryItemData(reader.ReadInt32Packed(),reader.ReadInt32Packed(),reader.ReadInt32Packed(), reader.ReadByteDirect());
		    }
		});
	}

	public virtual void InitializeBuffs(){
		buffList = _buffList;
		for(int i = 0; i < buffList.Count; i++){
			buffList[i].typeId = i;
		}
	}

	public virtual void InitializeItems(){
		droppedItemPrefab = _droppedItemPrefab;
		itemList = _itemList;
		for(int i = 0; i < itemList.Count; i++){
			itemList[i].id = i;
		}
	}

    public virtual void OnApplicationQuit() {
    	if(NetworkingManager.Singleton.IsHost) 
    		NetworkingManager.Singleton.StopHost();
    	else if (NetworkingManager.Singleton.IsServer) 
    		NetworkingManager.Singleton.StopServer();
    	else if(NetworkingManager.Singleton.IsClient) 
    		NetworkingManager.Singleton.StopClient();
    }
}
