﻿using UnityEngine;
using UnityEngine.UI;

public class MessageLogUI : MonoBehaviour
{
	public static MessageLogUI instance;
	public Transform container;
	public GameObject prefab;

	public void Awake(){
		instance = this;
	}

	public static void ShowMessage(string text){
		GameObject go = Instantiate(instance.prefab, instance.container);
		go.GetComponent<Text>().text = text;
	}
}
