﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MLAPI;
using MLAPI.Connection;
using MLAPI.Transports;
using MLAPI.Security;
using MLAPI.Spawning;

public class GameUI : MonoBehaviour
{
    public InputField ipInput;
    public GameObject connectingPanel;
    public GameObject gameMenu;
    public GameObject startMenu;
    public GameObject inventoryMenu;

    public bool connected {
    	get {
    		return NetworkingManager.Singleton.IsHost
    			|| NetworkingManager.Singleton.IsServer
    			|| (NetworkingManager.Singleton.IsClient && NetworkingManager.Singleton.IsConnectedClient);
    	}
    }

    public void Start(){
    	ipInput.text = NetworkingManager.Singleton.GetComponent<EnetTransport.EnetTransport>().Address;
    }

    public void StartClient(){
    	Disconnect();
    	connectingPanel.SetActive(true);
    	NetworkingManager.Singleton.GetComponent<EnetTransport.EnetTransport>().Address = ipInput.text;
    	NetworkingManager.Singleton.StartClient();
    }

    public void ClientConnected(){
    	connectingPanel.SetActive(false);
    }

    public void StartServer(){
    	Disconnect();
    	NetworkingManager.Singleton.StartServer();
    	startMenu.SetActive(false);
    }

    public void StartHost(){
    	Disconnect();
    	NetworkingManager.Singleton.StartHost();
    	startMenu.SetActive(false);
    }

    public void Disconnect(){
    	if(NetworkingManager.Singleton.IsHost) 
    		NetworkingManager.Singleton.StopHost();
    	else if (NetworkingManager.Singleton.IsServer) 
    		NetworkingManager.Singleton.StopServer();
    	else if(NetworkingManager.Singleton.IsClient) 
    		NetworkingManager.Singleton.StopClient();
    	connectingPanel.SetActive(false);
    	gameMenu.SetActive(false);
    }

    public void Update(){
    	//gameMenu
    	if(connected){
            connectingPanel.SetActive(false);
            startMenu.SetActive(false);
    		if(Input.GetKeyDown(KeyCode.Escape)){
    			gameMenu.SetActive(!gameMenu.activeSelf);
    		}

            if(!gameMenu.activeSelf){
                if(Input.GetButtonDown("Inventory")){
                    inventoryMenu.SetActive(!inventoryMenu.activeSelf);
                }
            }
		} else {
            inventoryMenu.SetActive(false);
			gameMenu.SetActive(false);
			startMenu.SetActive(true);
		}
    }

    public void Quit(){
    	Debug.Log("Quit");
    }
}
