﻿using UnityEngine;
using UnityEngine.UI;

public class MessageLogTextUI : MonoBehaviour
{
    public Color startColor;
    public Color endColor;
    public float timer = 5;
    public Text text;
    float _timer;

    void Start() {
        _timer = timer;
        text.color = startColor;
    }

    void Update(){
        _timer -= Time.deltaTime;
        if(_timer < 0){
        	Destroy(gameObject);
        } else {
        	text.color = Color.Lerp(endColor, startColor, _timer/timer);
        }
    }
}
