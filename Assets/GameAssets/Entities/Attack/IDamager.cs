﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamager {
	Damage damage { get; set; }
}