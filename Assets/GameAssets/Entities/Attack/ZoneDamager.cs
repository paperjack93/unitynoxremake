﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;

public class ZoneDamager : MonoBehaviour, IDamager
{
	public Damage damage {get{return _damage;} set{_damage = value;}}
    [SerializeField] private Damage _damage;
	public LayerMask mask;
	public float size = 1;

    void Start(){
        if(!NetworkingManager.Singleton.IsServer) return;
        Collider[] hitColliders = 
    		Physics.OverlapSphere(
    			transform.position,
    			size, 
    			mask
    		);
    	if(hitColliders.Length > 0){
    		foreach(Collider collider in hitColliders){
                AttackSystem.ApplyDamage(collider.gameObject, damage);
    		}
    	}
    }
}