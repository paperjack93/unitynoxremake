﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Damage {
	[System.Serializable]
	public enum Type {
		Blade, 
		Crush, 
		Impale, 
		Poison, 
		Explosion, 
		Drain,
		Flame, 
		Electrical,
		Heal,
		//DispelUndead, //Commented until needed 
		Death,
		Mana
	};
    [HideInInspector] public BaseNetworkedEntity source;
    public Type type;
    public bool ignoreSource = true;
	public int amount;
}
