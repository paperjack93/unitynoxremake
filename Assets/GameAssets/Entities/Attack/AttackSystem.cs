﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;

public class AttackSystem : MonoBehaviour
{
	public bool canAttack { get { return coolDown == 0f; } }
	public float attackDelay = 0f;
	public float coolDown = 0f;
	public float stopMovementTime = 0f;

	public BaseNetworkedEntity owner;

	public Attack attack;

    public void Attack(){
    	if(!canAttack) return;
    	StartCoroutine(AttackAsync());    	
    }

    protected virtual IEnumerator AttackAsync(){
    	//Attack attack = GetAttack();
    	attack.source = owner;
    	attack.damage.source = owner;
		stopMovementTime = attack.stopMovementTime;
    	coolDown = attack.coolDown;
    	attackDelay = attack.attackDelay;

    	if(NetworkingManager.Singleton.IsServer) owner.animation.SetAttack();

    	yield return new WaitForSeconds(attackDelay);

    	GameObject gameObject = Instantiate(
    		attack.prefab, 
    		transform.position + transform.TransformDirection(new Vector3(0,1,1)),
    		Quaternion.LookRotation(transform.forward, Vector3.up)
    	);
    	if(gameObject.TryGetComponent(out IDamager damager)){
			damager.damage = attack.damage;
		} 
    }

    public static bool ApplyDamage(GameObject gameObject, Damage damage){
    	if(gameObject.TryGetComponent(out BaseNetworkedEntity entity)){
			if(damage.ignoreSource && entity == damage.source) return false;
		} 
		if(gameObject.TryGetComponent(out Health health)){
			health.amount -= damage.amount;
			return true;
		}
		return false;
    }

	void Update(){
		//This is here to allow stuff to modify the timers in runtime
		if(coolDown > 0) {
			coolDown = Mathf.MoveTowards(coolDown, 0, Time.deltaTime);
		}
		if(stopMovementTime > 0) {
			stopMovementTime = Mathf.MoveTowards(stopMovementTime, 0, Time.deltaTime);
		}
	}
}
