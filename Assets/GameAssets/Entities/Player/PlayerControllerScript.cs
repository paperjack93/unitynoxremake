using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using MLAPI.NetworkedVar;
using MLAPI.Messaging;

public class PlayerControllerScript : NetworkedBehaviour
{
    public static float inputMouseDirectionResolution = 10;
    public static float inputLookMaxDistance = 2;
    public static float inputInteractionDistance = 2;
    public static Vector3 worldPosUnderMouse;
    public static PlayerControllerScript localPlayer;

    public GameObject playerEntityPrefab;
    public LayerMask interactableMask;
    public LayerMask worldMask;

    [SyncedVar]
    public NetworkedObject playerEntity;

    public bool _inputReceived = false;
    public bool _connected = false;

    public PlayerInventory inventory;

	public NetworkedVarUShort inputMouseDirection = new NetworkedVarUShort(new NetworkedVarSettings() {
        SendChannel = "us", // The var value will be synced over this channel
        ReadPermission = NetworkedVarPermission.ServerOnly, // The var values will be synced to everyone
        SendTickrate = 15, // The var will sync no more than 2 times per second
        WritePermission = NetworkedVarPermission.OwnerOnly, // Only the owner of this object is allowed to change the value
    } );

	public NetworkedVarByte inputMouseDistance = new NetworkedVarByte(new NetworkedVarSettings() {
        SendChannel = "us", // The var value will be synced over this channel
        ReadPermission = NetworkedVarPermission.ServerOnly, // The var values will be synced to everyone
        SendTickrate = 15, // The var will sync no more than 2 times per second
        WritePermission = NetworkedVarPermission.OwnerOnly, // Only the owner of this object is allowed to change the value
    } );

    public NetworkedVarBool inputMove = new NetworkedVarBool(new NetworkedVarSettings() {
            SendChannel = "us", // The var value will be synced over this channel
            ReadPermission = NetworkedVarPermission.ServerOnly, // The var values will be synced to everyone
            SendTickrate = 15, // The var will sync no more than 2 times per second
            WritePermission = NetworkedVarPermission.OwnerOnly, // Only the owner of this object is allowed to change the value
    } );

    public override void NetworkStart(){
    	base.NetworkStart();
        if(IsServer) ServerStart();
        if(IsOwner) localPlayer = this;

        if(IsServer){
            inventory.AddItem(new InventoryItemData(0, 1, 0));
            inventory.AddItem(new InventoryItemData(0, 1, 0));
            inventory.AddItem(new InventoryItemData(0, 1, 0));
            inventory.AddItem(new InventoryItemData(1, 1, 0));
            inventory.AddItem(new InventoryItemData(1, 1, 0));
        }
    }

    public void ServerStart(){
        StartCoroutine(WaitForPlayerSpawnSignal());
    }

    protected virtual void OnDestroy(){
        _connected = false;
        if(IsServer) OnServerDisconnected();
        if(IsOwner) localPlayer = null;
    }

    protected virtual void OnServerDisconnected(){
        //Kill the player entity on disconnect
        if(playerEntity) {
            PlayerEntityScript entityScript = playerEntity.GetComponent<PlayerEntityScript>();
            entityScript.health.OnEmpty.RemoveListener(OnPlayerEntityDeath);
            entityScript.health.amount = 0;
        }
    }

    private IEnumerator WaitForPlayerSpawnSignal(){
        playerEntity = null;

        while(!_inputReceived){
            yield return null;
        }

        SpawnPlayerEntity();
    }

    private void SpawnPlayerEntity(){
        GameObject go = Instantiate(playerEntityPrefab, Vector3.zero, Quaternion.identity);
        playerEntity = go.GetComponent<NetworkedObject>();

        PlayerEntityScript entityScript = playerEntity.GetComponent<PlayerEntityScript>();
        entityScript.playerController = NetworkedObject;                
        entityScript.health.OnEmpty.AddListener(OnPlayerEntityDeath);
        PlayerEquipment equipmentScript = playerEntity.GetComponent<PlayerEquipment>();
        inventory.equipment = equipmentScript;
        equipmentScript.inventory = inventory;

        playerEntity.Spawn();

        GameObject test = Instantiate(GameManager.droppedItemPrefab, Vector3.zero, Quaternion.identity);
        test.GetComponent<NetworkedObject>().Spawn();
    }

    private void OnPlayerEntityDeath(){
        StartCoroutine(WaitForPlayerSpawnSignal());
    }   

    protected virtual void Update(){
        if(IsServer) ServerUpdate();
        if(IsOwner) OwnerUpdate();
    }

    protected virtual void LateUpdate(){
        if(_inputReceived) _inputReceived = false;
    }

    protected virtual void ServerUpdate(){
        if(playerEntity){
            float angle = ((float)inputMouseDirection.Value)/inputMouseDirectionResolution;
            Quaternion rot = Quaternion.Euler(0,angle,0);
            //Vector3 fwd = rot * Vector3.forward;
            PlayerEntityScript entityScript = playerEntity.GetComponent<PlayerEntityScript>();
            entityScript.SetRotation(angle);
            if(inputMove.Value){
                entityScript.SetMove(inputMouseDistance.Value);
            } else {
                entityScript.SetMove(0);
            }
        }        
    }

    protected virtual void OwnerUpdate(){
        if(playerEntity){
            // Oblique projection does wonky things... TODO: Move this to a helper class
            Vector3 cameraFwd = Camera.main.transform.forward;

            Vector3 obliqueScreenPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 centerScreenPosition = Camera.main.ViewportToWorldPoint(new Vector3(0.5f,0.5f,0));
            Vector3 mouseScreenPosition = Camera.main.transform.position + (obliqueScreenPosition - centerScreenPosition);
            Ray ray = new Ray(mouseScreenPosition, cameraFwd);
            Plane worldPlane = new Plane(Vector3.up, Vector3.zero);
            float mouseHitDistance;
            
            if(worldPlane.Raycast(ray, out mouseHitDistance)){
                Vector3 mouseWorldPosition = ray.GetPoint(mouseHitDistance);
                Vector3 direction = mouseWorldPosition - playerEntity.transform.position;
                Quaternion rotation = Quaternion.LookRotation(direction);
                inputMouseDirection.Value = (ushort)Mathf.RoundToInt(rotation.eulerAngles.y * inputMouseDirectionResolution);
                inputMove.Value = Input.GetMouseButton(1);
                if(Input.GetMouseButtonDown(1)) inputMouseDistance.Value = 0;
                if(inputMove.Value){
                    float distance = direction.magnitude;
                    float normalized = Mathf.Min(distance, inputLookMaxDistance)/inputLookMaxDistance;
                    inputMouseDistance.Value = (byte) Mathf.Max(inputMouseDistance.Value, Mathf.FloorToInt(normalized*254));
                }

                RaycastHit hit;
                Debug.DrawRay(ray.origin, ray.direction*100, Color.green, 1);
                if(Physics.Raycast(ray, out hit, Mathf.Infinity, interactableMask, QueryTriggerInteraction.Collide)) 
                    InteractionCast(hit);
                else if(Input.GetMouseButtonDown(0)) 
                    InvokeServerRpc(InputCMD, (byte)InputType.Attack);

                if(Physics.Raycast(ray, out hit, Mathf.Infinity, worldMask, QueryTriggerInteraction.Ignore))
                    worldPosUnderMouse = hit.point; 

                if(Input.GetButtonDown("Jump")) InvokeServerRpc(InputCMD, (byte)InputType.Jump);

                if(Input.GetButtonDown("Skill 1")) InvokeServerRpc(InputCMD, (byte)InputType.Skill0);
                if(Input.GetButtonDown("Skill 2")) InvokeServerRpc(InputCMD, (byte)InputType.Skill1);
                if(Input.GetButtonDown("Skill 3")) InvokeServerRpc(InputCMD, (byte)InputType.Skill2);
                if(Input.GetButtonDown("Skill 4")) InvokeServerRpc(InputCMD, (byte)InputType.Skill3);
                if(Input.GetButtonDown("Skill 5")) InvokeServerRpc(InputCMD, (byte)InputType.Skill4);
                if(Input.GetButtonDown("Skill 6")) InvokeServerRpc(InputCMD, (byte)InputType.Skill5);
                if(Input.GetButtonDown("Skill 7")) InvokeServerRpc(InputCMD, (byte)InputType.Skill6);
                if(Input.GetButtonDown("Skill 8")) InvokeServerRpc(InputCMD, (byte)InputType.Skill7);
                //if(Input.GetButtonDown("Skill 9")) InvokeServerRpc(InputCMD, (byte)InputType.Skill8);
            }                
        } else {
            if(Input.GetMouseButtonDown(0)) {
                InvokeServerRpc(InputCMD, (byte)InputType.Attack);
            }
            if(Input.GetButtonDown("Jump")) InvokeServerRpc(InputCMD, (byte)InputType.Jump);
        }
    }

    public enum InputType {
        Attack,
        Jump,
        Skill0,
        Skill1,
        Skill2,
        Skill3,
        Skill4,
        Skill5,
        Skill6,
        Skill7,
        Skill8,
    };

    [ServerRPC]
    void InputCMD(byte id) {
        if(playerEntity){
            switch((InputType)id){
                case InputType.Attack: InputAttack(); break;
                case InputType.Jump: InputJump(); break;
                case InputType.Skill0: InputSkill(0); break;
                case InputType.Skill1: InputSkill(1); break;
                case InputType.Skill2: InputSkill(2); break;
                case InputType.Skill3: InputSkill(3); break;
                case InputType.Skill4: InputSkill(4); break;
                case InputType.Skill5: InputSkill(5); break;
                case InputType.Skill6: InputSkill(6); break;
                case InputType.Skill7: InputSkill(7); break;
                case InputType.Skill8: InputSkill(8); break;
            }
        } else {

        }
        _inputReceived = true;
    }

    void InputAttack() {
        PlayerEntityScript entityScript = playerEntity.GetComponent<PlayerEntityScript>();
        if(!entityScript.attacks.canAttack) return;
        if(entityScript.ignorePlayerInput) return;
        entityScript.attacks.Attack();
        entityScript.InvokeClientRpcOnEveryone(entityScript.AttackRPC);
    }

    void InputJump() {
        PlayerEntityScript entityScript = playerEntity.GetComponent<PlayerEntityScript>();
        entityScript.Jump();
    }

    void InputSkill(int n) {
        PlayerEntityScript entityScript = playerEntity.GetComponent<PlayerEntityScript>();
        entityScript.playerSkills.UsePlayerSkill(n);
    }

    public void TryUseItem(int itemIndex){
        if(inventory.GetDataAtIndex(itemIndex) == null) return;
        if(inventory.inventorySync[itemIndex].id < 0) return;
        InvokeServerRpc(InputUseItemCMD, itemIndex);
    }

    [ServerRPC]
    void InputUseItemCMD(int itemIndex) {
        if(inventory.GetDataAtIndex(itemIndex) == null) return;
        if(inventory.inventorySync[itemIndex].id < 0) return;
        inventory.inventory[itemIndex].OnItemUsed();
    }

    void InteractionCast(RaycastHit hit){
        if(Vector3.Distance(playerEntity.transform.position, hit.transform.position) > inputInteractionDistance) return;
        if(!Input.GetMouseButtonDown(0)) return;

        if(hit.transform.TryGetComponent(out Interactable interactable)){
            InvokeServerRpc(InputInteractCMD, interactable.GetComponent<NetworkedObject>());
        }
    }

    [ServerRPC]
    void InputInteractCMD(NetworkedObject netObject) {
        Interactable interactable = netObject.GetComponent<Interactable>();
        if(!interactable) return;
        if(Vector3.Distance(playerEntity.transform.position, interactable.transform.position)  > inputInteractionDistance) return;
        
        interactable.Interact(this);
    }
}
