﻿using UnityEngine;
using MLAPI;
using MLAPI.Serialization;
using MLAPI.Serialization.Pooled;
using MLAPI.NetworkedVar;
using MLAPI.NetworkedVar.Collections;
using MLAPI.Messaging;

public class PlayerInventory : InventorySystem
{
	public PlayerEquipment equipment {
        get { return _equipment.Value; }
        set { _equipment.Value = value; }
    }
    
    NetworkedVar<PlayerEquipment> _equipment = new NetworkedVar<PlayerEquipment>(new NetworkedVarSettings {
        WritePermission = NetworkedVarPermission.ServerOnly,
        ReadPermission = NetworkedVarPermission.Everyone,
    }, null);

    public override void RemoveItem(InventoryItem item){
        if(IsServer && item is EquippableItem){
            EquippableItem ei = (EquippableItem) item;
            if(ei.equipped) equipment.UnequipItem(ei.slot);
        }
        base.RemoveItem(item);
    }

    public override void ApplyItemFlag(InventoryItem item, byte flag){
        switch(flag){
            case 1: //Item is equipped
                EquippableItem ei = (EquippableItem)item;
                equipment.EquipItem(ei);
            break;
            default:
            break;
        }
    }

    public void SwitchPositions(int from, int to){
        if(IsServer) SwitchPositionsCMD(from, to);
        else InvokeServerRpc(SwitchPositionsCMD, from, to);  
    }

    [ServerRPC]
    void SwitchPositionsCMD(int from, int to) {
        Debug.Log("Switch items from: "+from+" to: "+to);
        InventoryItemData fromData = GetDataAtIndex(from);
        InventoryItemData toData = GetDataAtIndex(to);
        
        AddItem(toData, from);
        AddItem(fromData, to);
    }

    /*void OnGUI(){
        int i = 0;
        foreach(int key in inventorySync.Keys){
            GUI.Label(new Rect(10, 10+(i*30), 250, 20), key+" " +inventorySync[key].ToString());
            i+=1;
        }
        foreach(PlayerEquipment.Slot key in equipment.equipment.Keys){
            GUI.Label(new Rect(10, 10+(i*30), 250, 20), key+" " +equipment.equipment[key].ToString());
            i+=1;
        }    
    }*/

    public override Vector3 GetDropPoint(){
        return equipment.transform.position;
    }
}
