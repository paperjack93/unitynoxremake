﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentGraphic : MonoBehaviour
{
	public PlayerEquipment equipment;
	public PlayerEquipment.Slot slot;
	public new string name;

	//TODO: a better way to do this
    void Awake() {
    	equipment = GetComponentInParent<PlayerEquipment>();
    	if(!equipment.availableGraphics.ContainsKey(slot)){
    		equipment.availableGraphics[slot] = new Dictionary<string, GameObject>();
    	}
    	equipment.availableGraphics[slot][name] = gameObject;
    	gameObject.SetActive(false);
    }
}
