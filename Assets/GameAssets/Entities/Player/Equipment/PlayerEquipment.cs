﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using MLAPI.Serialization;
using MLAPI.Serialization.Pooled;
using MLAPI.NetworkedVar;
using MLAPI.NetworkedVar.Collections;
using MLAPI.Messaging;

public class PlayerEquipment : NetworkedBehaviour
{
    public PlayerEntityScript playerEntity;
    public PlayerInventory inventory {
        get { return _inventory.Value; }
        set { _inventory.Value = value; }
    }
    
    NetworkedVar<PlayerInventory> _inventory = new NetworkedVar<PlayerInventory>(new NetworkedVarSettings {
        WritePermission = NetworkedVarPermission.ServerOnly,
        ReadPermission = NetworkedVarPermission.Everyone,
        SendTickrate = 0
    }, null);

    public enum Slot {
        Head,
        Arms,
        Body,
        Legs,
        LeftHand,
        RightHand
    } //Change UpdateGraphics count on change

    public NetworkedDictionary<Slot, InventoryItemData> equipment = new NetworkedDictionary<Slot, InventoryItemData>(new MLAPI.NetworkedVar.NetworkedVarSettings() {
        ReadPermission = MLAPI.NetworkedVar.NetworkedVarPermission.Everyone,
        WritePermission = MLAPI.NetworkedVar.NetworkedVarPermission.ServerOnly,
        SendTickrate = 0
    }, new Dictionary<Slot, InventoryItemData>());

    Dictionary<Slot, InventoryItemData> _currentData = new Dictionary<Slot, InventoryItemData>();
    Dictionary<Slot, GameObject> _currentWearables = new Dictionary<Slot, GameObject>();
    
    [SerializeField] public Dictionary<Slot, Dictionary<string, GameObject>> availableGraphics = new Dictionary<Slot, Dictionary<string, GameObject>>();

    void Awake(){
        equipment.OnDictionaryChanged += OnDictionaryChanged;
    }

    public override void NetworkStart(){
        base.NetworkStart();
        UpdateGraphics();
    }

    void OnDictionaryChanged(NetworkedDictionaryEvent<Slot, InventoryItemData> e){
        string name;
        switch (e.eventType){
            case NetworkedDictionaryEvent<Slot, InventoryItemData>.NetworkedListEventType.Add:
                if(e.value == null) {
                    DisableWearable(e.key);
                } else {
                    DisableWearable(e.key);
                    name = ((EquippableItem)GameManager.itemList[e.value.id]).graphicName;
                    EnableWearable(e.key, name);
                }
            break;
            case NetworkedDictionaryEvent<Slot, InventoryItemData>.NetworkedListEventType.Remove:
                DisableWearable(e.key);
            break;
            case NetworkedDictionaryEvent<Slot, InventoryItemData>.NetworkedListEventType.RemovePair:
                DisableWearable(e.key);
            break;
            case NetworkedDictionaryEvent<Slot, InventoryItemData>.NetworkedListEventType.Clear:
                foreach(Slot s in _currentWearables.Keys) DisableWearable(s);
            break;
            case NetworkedDictionaryEvent<Slot, InventoryItemData>.NetworkedListEventType.Value:
                DisableWearable(e.key);
                name = ((EquippableItem)GameManager.itemList[e.value.id]).graphicName;
                EnableWearable(e.key, name);
            break;
        }
    }

    void EnableWearable(Slot slot, string name){
        if(!availableGraphics.ContainsKey(slot) || !availableGraphics[slot].ContainsKey(name)) return;

        _currentWearables[slot] = availableGraphics[slot][name];
        _currentWearables[slot].SetActive(true);

        if(playerEntity.IsLocalPlayer) {
            if(_currentData.ContainsKey(slot) && _currentData[slot] != null) {
                InventoryItem item = inventory.GetItemAtIndex(_currentData[slot].index);
                if(item)
                    ((EquippableItem)item).equipped = false;                
            }
            _currentData[slot] = equipment[slot];
            GetEquipmentInSlot(slot).equipped = true;
            inventory.OnUpdated.Invoke();
        }
    }

    void DisableWearable(Slot slot){
        if(!_currentWearables.ContainsKey(slot) || _currentWearables[slot] == null) return;
        _currentWearables[slot].SetActive(false);
        _currentWearables.Remove(slot);

        if(playerEntity.IsLocalPlayer){
            if(_currentData.ContainsKey(slot) && _currentData[slot] != null) {
                InventoryItem ii = inventory.GetItemAtIndex(_currentData[slot].index);
                if(ii == null) return;
                if(ii is EquippableItem) ((EquippableItem)ii).equipped = false;
                _currentData.Remove(slot);
            }
        }
    }

    public bool HasEquipmentInSlot(Slot slot){
        return equipment.ContainsKey(slot) && equipment[slot] != null;
    }

    public EquippableItem GetEquipmentInSlot(Slot slot){
        if(!HasEquipmentInSlot(slot)) return null;
        InventoryItemData itemData = equipment[slot];
        if(IsServer || playerEntity.IsLocalPlayer) {
            InventoryItem item = inventory.GetItemAtIndex(itemData.index);
            if(item) return (EquippableItem) item;
            else return null;
        } else {
            // Other clients can just use a copy for reference purpose
            return (EquippableItem) GameManager.itemList[itemData.id];
        }
    }

    public void EquipItem(EquippableItem item){   
        if(HasEquipmentInSlot(item.slot))
            UnequipItem(item.slot);
        
        InventoryItemData data = inventory.GetDataAtIndex(item.index);
        data.flag = 1;
        item.equipped = true;
        equipment[item.slot] = new InventoryItemData(item.id, item.count, item.index);
        inventory.OnUpdated.Invoke();       
    }

    public void UnequipItem(Slot slot){
        if(!HasEquipmentInSlot(slot)) return;
        EquippableItem item = GetEquipmentInSlot(slot);
        if(item){
            if(IsServer){
                item.equipped = false;
                InventoryItemData data = inventory.GetDataAtIndex(item.index);
                if(data != null) data.flag = 0;
            } else if(playerEntity.IsLocalPlayer) 
                item.equipped = false;
        }
        equipment.Remove(slot);
        inventory.OnUpdated.Invoke();
    }

    public void UpdateGraphics(){
        foreach(GameObject go in _currentWearables.Values) if(go) go.SetActive(false);
        foreach(Slot slot in equipment.Keys){
            DisableWearable(slot);
            if(equipment[slot] == null) continue;
            string name = ((EquippableItem)GameManager.itemList[equipment[slot].id]).graphicName;
            EnableWearable(slot, name);
        }
        foreach(GameObject go in _currentWearables.Values) 
            if(go != null) go.SetActive(true);
    }
}
