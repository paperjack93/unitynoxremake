﻿using UnityEngine;
using MLAPI;
using MLAPI.NetworkedVar;
using MLAPI.Messaging;

public class PlayerEntityScript : BaseMovingEntity
{
	[SyncedVar]
    public NetworkedObject playerController;

    public new bool IsLocalPlayer = false;
    public bool ignorePlayerInput = false;
    public Mana mana;
    public PlayerSkillSystem playerSkills;

    public BaseMovingEntity.Impulse jumpImpulse;

	protected override void ServerStart(){
		base.ServerStart();
		health.OnEmpty.AddListener(Death);
    }

	protected override void ClientStart(){
		base.ClientStart();
		IsLocalPlayer = playerController.IsLocalPlayer;
		if(IsLocalPlayer){
            PlayerUI.target = this;
			PlayerCameraController.instance.target = transform;
            transform.Find("ShadowExtrusionCenter").gameObject.SetActive(true);
		}
    }

    public void SetRotation(float n){
        if(ignorePlayerInput) return;
        targetRotation = n;
    }

    public void SetMove(byte n){
        if(ignorePlayerInput) return;
        float mouseDistance = (float)n/254f;
    	direction = transform.forward * mouseDistance;
        animation.SetSpeed(n);
    }

    public void Jump(){
        if(ignorePlayerInput) return;
        if(isGrounded) AddImpulse(jumpImpulse, transform.forward);
    }

    public void Death(){
    	Destroy(gameObject);
    }

	[ClientRPC]
    public void AttackRPC() {
        attacks.Attack();
    }
}
