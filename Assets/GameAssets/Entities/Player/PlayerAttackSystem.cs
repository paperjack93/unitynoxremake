﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackSystem : AttackSystem
{
	public PlayerEquipment equipment;
    public Attack defaultAttack;

    protected override IEnumerator AttackAsync(){
    	EquippableItem weapon = equipment.GetEquipmentInSlot(PlayerEquipment.Slot.RightHand);
    	if(weapon && weapon is WeaponItem){
    		attack = ((WeaponItem)weapon).attack;
    	} else {
    		attack = defaultAttack;
    	}    	
    	return base.AttackAsync();
    }
}
