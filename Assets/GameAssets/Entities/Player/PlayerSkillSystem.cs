﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkillSystem : SkillSystem {
	[System.Serializable]
	public class PlayerSkillIndexesPage {
		public int[] indexes = new int[8];
		public int this[int index]{
			get => indexes[index];
        	set => indexes[index] = value;
		}
	}
	public int skillPage = 0;
	public List<PlayerSkillIndexesPage> playerSkillIndexes = new List<PlayerSkillIndexesPage>();

	public void UsePlayerSkill(int n){
    	if(!IsServer) return;
		if(playerSkillIndexes[skillPage][n] == -1) return;

		UseSkill(playerSkillIndexes[skillPage][n]);
	}

	public BaseSkill GetSkillAtSlot(int i){
		int n = playerSkillIndexes[skillPage][i];
		if(n > availableSkills.Count) return null;
		if(n < 0) return null;
		return availableSkills[n];
	}
}
