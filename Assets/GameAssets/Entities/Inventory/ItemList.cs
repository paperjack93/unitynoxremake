﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemList : ScriptableObject {
	public List<InventoryItem> items = new List<InventoryItem>();

	public InventoryItem this[int i] {
		get { return items[i]; }
		set { items[i] = value; }
	}

	public int Count {
		get { return items.Count; }
	}

	public InventoryItem GetInstance(int i){
		return ScriptableObject.Instantiate(this[i]);
	}
}
