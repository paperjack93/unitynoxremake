﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Items/Weapon Item")]
public class WeaponItem : EquippableItem {
	public Attack attack;
}
