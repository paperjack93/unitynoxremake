﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MLAPI;
using MLAPI.Serialization;
using MLAPI.Serialization.Pooled;
using MLAPI.NetworkedVar;
using MLAPI.NetworkedVar.Collections;
using MLAPI.Messaging;

public class InventorySystem : NetworkedBehaviour
{
    public static float dropMaxDistance = 2;
    public UnityEvent OnUpdated = new UnityEvent();

    public NetworkedDictionary<int, InventoryItemData> inventorySync = new NetworkedDictionary<int, InventoryItemData>(new MLAPI.NetworkedVar.NetworkedVarSettings() {
        ReadPermission = MLAPI.NetworkedVar.NetworkedVarPermission.Everyone,
        WritePermission = MLAPI.NetworkedVar.NetworkedVarPermission.ServerOnly,
        SendTickrate = 0
    }, new Dictionary<int, InventoryItemData>());

    public Dictionary<int, InventoryItem> inventory = new Dictionary<int, InventoryItem>();
    public PlayerControllerScript controller;

    void Awake(){
        inventorySync.OnDictionaryChanged += OnDictionaryChanged;
    }

    public InventoryItemData GetDataAtIndex(int i){
        if(inventorySync.ContainsKey(i)) return inventorySync[i];
        else return null;
    }

    public InventoryItem GetItemAtIndex(int i){
        if(inventory.ContainsKey(i)) return inventory[i];
        else return null;
    }

    public int FindOpenSlot(){
        int n = 0;
        InventoryItemData data = null;
        while(true){
            data = GetDataAtIndex(n);
            if(data == null) return n;
            n+= 1;
        }
    }

    public void AddItem(InventoryItem item){
        int index = FindOpenSlot();
        AddItem(new InventoryItemData(item.id, item.count, index), index);
    }

    public void AddItem(InventoryItemData item){
        int index = FindOpenSlot();
        item.index = index;
        AddItem(item, index);
    }

	public void AddItem(InventoryItem item, int index){
    	AddItem(new InventoryItemData(item.id, item.count, index), index);
    }

    public void AddItem(InventoryItemData item, int index){
    	if(!IsServer) return;
        if(item != null) {
            item.index = index;
            inventorySync[index] = item;
        } else inventorySync.Remove(index);
    }

    void OnDictionaryChanged(NetworkedDictionaryEvent<int, InventoryItemData> e){
        switch (e.eventType){
            case NetworkedDictionaryEvent<int, InventoryItemData>.NetworkedListEventType.Add:
                SyncInventorySlot(e.key);
            break;
            case NetworkedDictionaryEvent<int, InventoryItemData>.NetworkedListEventType.Remove:
                SyncInventorySlot(e.key);
            break;
            case NetworkedDictionaryEvent<int, InventoryItemData>.NetworkedListEventType.RemovePair:
                SyncInventorySlot(e.key);
            break;
            case NetworkedDictionaryEvent<int, InventoryItemData>.NetworkedListEventType.Clear:
                inventory.Clear();
            break;
            case NetworkedDictionaryEvent<int, InventoryItemData>.NetworkedListEventType.Value:
                SyncInventorySlot(e.key);
            break;
        }
        OnUpdated.Invoke();
    }

    void SyncInventorySlot(int slot){
        InventoryItemData data = GetDataAtIndex(slot);
        
        if(data == null) {
            Debug.Log("sync: "+slot+ " clear");
            inventory.Remove(slot);
        } else {
            Debug.Log("sync: "+slot+ " "+data);
            InventoryItem item = GameManager.itemList.GetInstance(data.id);
            item.count = data.count;
            item.owner = controller;
            item.inventory = this;
            item.index = slot;
            inventory[slot] = item;
            ApplyItemFlag(item, data.flag);
        }
    }

    public virtual void ApplyItemFlag(InventoryItem item, byte flag){
        switch(flag){
            default:
            break;
        }
    }
    public virtual void RemoveItem(InventoryItem item){
        RemoveItem(GetDataAtIndex(item.index));
    }

    public virtual void RemoveItem(InventoryItemData item){
        if(item==null) return;
        inventory.Remove(item.index);
        if(IsServer) {
            inventorySync.Remove(item.index);
            OnUpdated.Invoke();
        }
    }

    public virtual void DropItem(InventoryItem item, Vector3 position){
        DropItem(GetDataAtIndex(item.index), position);
    }

    public virtual void DropItem(InventoryItemData item, Vector3 position){
        if(item == null) return;
        InvokeServerRpc(DropItemCMD, item.index, position);
    }

    public virtual Vector3 GetDropPoint(){
        return transform.position;
    }

    [ServerRPC]
    protected virtual void DropItemCMD(int index, Vector3 position) {
        if(GetDataAtIndex(index) == null) return;
        position = GetDropPoint() + Vector3.ClampMagnitude(position - GetDropPoint(), dropMaxDistance);
        GetItemAtIndex(index).OnDropped(position);
        GameObject drop = Instantiate(GameManager.droppedItemPrefab, position, Quaternion.identity);
        drop.GetComponent<DroppedItem>().item = GetItemAtIndex(index);
        drop.GetComponent<NetworkedObject>().Spawn();
        RemoveItem(GetDataAtIndex(index));
    }
}
