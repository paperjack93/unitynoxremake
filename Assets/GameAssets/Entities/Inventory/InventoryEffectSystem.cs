﻿using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Item Effects System")]
public class InventoryEffectSystem : ScriptableObject {
	public Damage healMidAmount;

	public void DoNothing(ConsumableItem item){

	}

	public void HealMid(ConsumableItem item){
		AttackSystem.ApplyDamage(item.owner.playerEntity.gameObject, healMidAmount);
		LowerCount(item);
	}

	public void LowerCount(InventoryItem item){
		item.inventory.inventorySync[item.index].count -= 1;
		if(item.inventory.inventorySync[item.index].count == 0){
			item.inventory.RemoveItem(item);
		}
	}
}
