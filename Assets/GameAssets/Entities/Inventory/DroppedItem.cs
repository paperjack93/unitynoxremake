﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroppedItem : Interactable
{
	public InventoryItem item;
	public void Start(){

	}

	public override void Interact(PlayerControllerScript interactor){
		base.Interact(interactor);
		interactor.inventory.AddItem(item);
		Destroy(gameObject);
	}
}
