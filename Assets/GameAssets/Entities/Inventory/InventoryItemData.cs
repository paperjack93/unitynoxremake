﻿[System.Serializable]
public class InventoryItemData {
	public int id = -1;
	public int count = 1;
	public int index = -1;
	public byte flag = 0;
	// 1 - is equipment and is equipped
	
	public InventoryItemData(int id, int amount, int index, byte flag=0){
		this.id = id;
		this.count = amount;
		this.index = index;
		this.flag = flag;
	}

	public override string ToString(){
		return "(id:"+id+" count:"+count+" index:"+index+")";
	}  

}
