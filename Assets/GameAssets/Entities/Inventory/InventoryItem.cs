﻿using UnityEngine;

[System.Serializable]
public class InventoryItem : ScriptableObject {
	[HideInInspector]
	public int id = -1;
	public int index = -1;
	public InventorySystem inventory;
	public PlayerControllerScript owner;

	public new string name;
	public string description;
	public Sprite icon;
	public int count = 1;
	public float weight = 10;
	public int value = 10;
	public bool consumable = false;
	public bool droppable = false;

	public virtual void OnItemUsed(){
		Debug.Log("You used an item, but it does nothing.");
	}

	public virtual void OnPickedUp(){
	}

	public virtual void OnDropped(Vector3 position){
	}

}
