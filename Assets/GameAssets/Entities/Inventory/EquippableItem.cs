﻿using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Items/Equippable Item")]
public class EquippableItem : InventoryItem
{
	public PlayerEquipment.Slot slot;
	public bool equipped = false;
	public int durability = 100;
	public string graphicName = "TestHat";

	public override void OnItemUsed(){
		if(equipped) OnUnequipped();
		else OnEquipped();
	}

	public virtual void OnEquipped(){
		((PlayerInventory)inventory).equipment.EquipItem(this);
	}

	public virtual void OnUnequipped(){
		((PlayerInventory)inventory).equipment.UnequipItem(slot);
	}

	public override void OnDropped(Vector3 position){
		base.OnDropped(position);
		((PlayerInventory)inventory).equipment.UnequipItem(slot);
	}
}
