﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class ConsumableItemEvent : UnityEvent<ConsumableItem> {}

[System.Serializable]
[CreateAssetMenu(menuName = "Items/Consumable Item")]
public class ConsumableItem : InventoryItem
{
	public ConsumableItemEvent UsedEvent;

	public override void OnItemUsed(){
		UsedEvent.Invoke(this);
	}
}
