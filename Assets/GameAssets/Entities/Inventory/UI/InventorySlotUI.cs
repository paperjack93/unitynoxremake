﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InventorySlotUI : MonoBehaviour, IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	public static float doubleClickTime = 0.5f;
    public RectTransform rt;
    public RectTransform dragRt;
    public Image image;
    public Image selectedIcon;
    public Image equippedIcon;
    public int index = -1;
    public InventoryItem item{
    	get {
    		return _item;
    	}
    	set {
    		_item = value;
    		image.gameObject.SetActive(true);
    	    image.sprite = _item.icon;

    	    if(_item is EquippableItem){
    	    	EquippableItem ei = (EquippableItem) _item;
    	    	if(ei.equipped) equippedIcon.gameObject.SetActive(true);
    	    }
    	}
    }
    [SerializeField] InventoryItem _item;
    float _lastClickTime = 0;
    Vector3 _dragOffset;

    public InventoryUI inventoryUI;

    public virtual void OnPointerClick(PointerEventData pointerEventData) {
    	if(_item == null) return;
    	if((_lastClickTime+doubleClickTime) > Time.time) OnDoubleClicked();
    	else OnClicked();

    	_lastClickTime = Time.time;
    }

    public virtual void OnClicked(){
    	Debug.Log("OnClicked");
    	inventoryUI.OnSlotSelected(this);
    }

    public virtual void OnDoubleClicked(){
    	Debug.Log("OnDoubleClicked");
    	inventoryUI.OnSlotDoubleClicked(this);
    }

    public virtual void OnSelected(){
    	selectedIcon.gameObject.SetActive(true);
    }

    public virtual void OnUnselected(){
    	selectedIcon.gameObject.SetActive(false);
    }

    public virtual void OnBeginDrag(PointerEventData data){
        if(_item == null) return;
        dragRt.SetParent(inventoryUI.dragContainer);
        Vector3 globalMousePos;
        RectTransformUtility.ScreenPointToWorldPointInRectangle(inventoryUI.dragContainer, data.position, data.pressEventCamera, out globalMousePos);
        Vector3 globalRtPos;
        RectTransformUtility.ScreenPointToWorldPointInRectangle(inventoryUI.dragContainer, dragRt.position, data.pressEventCamera, out globalRtPos);
        _dragOffset = globalRtPos - globalMousePos;
    }

    public virtual void OnDrag(PointerEventData data){
        if(_item == null) return;
        Vector3 globalMousePos;
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(inventoryUI.dragContainer, data.position, data.pressEventCamera, out globalMousePos)){
            dragRt.position = globalMousePos + _dragOffset;
        }
    }

    public virtual void OnEndDrag(PointerEventData data){
        if(_item == null) return;
        dragRt.SetParent(rt);
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(data, raycastResults);
        if(raycastResults.Count > 0) {
            foreach(RaycastResult r in raycastResults){
                if(r.gameObject.TryGetComponent(out InventorySlotUI slot)){
                    inventoryUI.SwitchPositions(index, slot.index);
                    Destroy(gameObject);
                    return;
                }
            }
            //No valid slots, reset
            dragRt.anchoredPosition = Vector2.zero;            
        } else {
            //Dropped outside of UI;
            inventoryUI.DropItem(item);
            Destroy(gameObject);
        }

    }
}
