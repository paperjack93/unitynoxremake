﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour
{
    public GameObject slotPrefab;
    public Transform container;
    public RectTransform dragContainer;
    public int minColumn = 8;
    public int minRow = 6;

    public Text itemNameText;
    public Text itemDescriptionText;

    public InventorySlotUI currentSelectedSlot;

    void OnEnable() {
		PlayerControllerScript.localPlayer.inventory.OnUpdated.AddListener(Rebuild);
		Rebuild();
	}

	void OnDisable(){
		PlayerControllerScript.localPlayer.inventory.OnUpdated.RemoveListener(Rebuild);
	}

	public void Rebuild(){
		Utils.KillChildren(container);
		int n = minColumn * minRow;

		PlayerInventory playerInventory = PlayerControllerScript.localPlayer.inventory;
		
		List<int> keys = new List<int>(playerInventory.inventory.Keys);
		
		if(keys.Count > 0){
			keys.Sort();
			int biggestKey = keys[keys.Count-1];
			while(n < biggestKey) n+= minColumn;
		}

		for(int i=0; i<n; i++){
			GameObject slot = Instantiate(slotPrefab,container);
			InventorySlotUI slotUI = slot.GetComponent<InventorySlotUI>();
			slotUI.index = i;
			if(keys.Count > 0 && i==keys[0]){
				slotUI.item = playerInventory.inventory[i];
				slotUI.inventoryUI = this;
				keys.RemoveAt(0);
			}
		}

		itemNameText.text = "";
		itemDescriptionText.text = "";

		currentSelectedSlot = null;
	}

	public void OnSlotSelected(InventorySlotUI slot){
		if(slot.item == null) return;

		itemNameText.text = slot.item.name;
		itemDescriptionText.text = slot.item.description;
		itemDescriptionText.text += "\n\n\n";
		itemDescriptionText.text += "Weight: "+slot.item.weight;
		itemDescriptionText.text += "\n";
		itemDescriptionText.text += "Value: "+slot.item.value;

		if(currentSelectedSlot) currentSelectedSlot.OnUnselected();
		currentSelectedSlot = slot;
		currentSelectedSlot.OnSelected();
	}

	public void OnSlotDoubleClicked(InventorySlotUI slot){
		if(slot.item == null) return;
		PlayerControllerScript.localPlayer.TryUseItem(slot.item.index);
	}

	public void SwitchPositions(int from, int to){
		PlayerInventory playerInventory = PlayerControllerScript.localPlayer.inventory;
		playerInventory.SwitchPositions(from, to);
	}

	public void DropItem(InventoryItem item){
		PlayerInventory playerInventory = PlayerControllerScript.localPlayer.inventory;
		playerInventory.DropItem(item, PlayerControllerScript.worldPosUnderMouse);
	}

}
