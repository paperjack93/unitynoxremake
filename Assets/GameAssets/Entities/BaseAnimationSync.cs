﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using MLAPI.NetworkedVar;
using MLAPI.Messaging;

public class BaseAnimationSync : NetworkedBehaviour
{
	//TODO: Sync animation parameters ONLY if in player's screen

	public Animator animator;
	public float speed {
		get{ return (float)_speed.Value/254f; }
	}

	public bool isGrounded {
		get{ return _grounded.Value; }
	}

	private NetworkedVarByte _speed = new NetworkedVarByte(new NetworkedVarSettings()
        {
            SendChannel = "us", // The var value will be synced over this channel
            ReadPermission = NetworkedVarPermission.Everyone, // The var values will be synced to everyone
            SendTickrate = 15, // The var will sync no more than 2 times per second
            WritePermission = NetworkedVarPermission.ServerOnly, // Only the owner of this object is allowed to change the value
        }
    );

	private NetworkedVarBool _grounded = new NetworkedVarBool(new NetworkedVarSettings()
        {
            SendChannel = "us", // The var value will be synced over this channel
            ReadPermission = NetworkedVarPermission.Everyone, // The var values will be synced to everyone
            SendTickrate = 15, // The var will sync no more than 2 times per second
            WritePermission = NetworkedVarPermission.ServerOnly, // Only the owner of this object is allowed to change the value
        }
    );

    public void Awake(){
    	_speed.OnValueChanged += OnSpeedChanged;
    	_grounded.OnValueChanged += OnGroundedChanged;
    }

	public void SetSpeed(byte n){
		_speed.Value = n;
	}

	public void SetGrounded(bool n){
		_grounded.Value = n;
	}

	void OnSpeedChanged(byte o, byte n){
		animator.SetFloat("Speed", speed);
	}

	void OnGroundedChanged(bool o, bool n){
		animator.SetBool("Grounded", isGrounded);
	}

	[ClientRPC]
	public void PlayAttackRPC(){
		animator.SetTrigger("Attack");
	}

	public void SetAttack(){
		InvokeClientRpcOnEveryone(PlayAttackRPC);
	}
}
