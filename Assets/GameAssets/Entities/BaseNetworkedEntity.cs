﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;

public class BaseNetworkedEntity : NetworkedBehaviour
{
	public CharacterController controller;
    public Health health;
    public AttackSystem attacks;
    public SkillSystem skills;
    public BuffSystem buffs;
    public new BaseAnimationSync animation;

	public override void NetworkStart(){
		base.NetworkStart();
        if(IsServer) ServerStart();
        if(IsClient) ClientStart();
    }

    protected virtual void ServerStart(){}

    protected virtual void ClientStart(){}

	protected virtual void Update(){
        if(IsServer) ServerUpdate();
        if(IsClient) ClientUpdate();
    }

    protected virtual void ServerUpdate(){}

    protected virtual void ClientUpdate(){}

    protected virtual void FixedUpdate(){
        if(IsServer) ServerFixedUpdate();
        if(IsClient) ClientFixedUpdate();
    }

    protected virtual void ServerFixedUpdate(){}

    protected virtual void ClientFixedUpdate(){}

}
