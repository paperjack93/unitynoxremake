﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using MLAPI.Serialization;
using MLAPI.Serialization.Pooled;
using MLAPI.NetworkedVar;
using MLAPI.NetworkedVar.Collections;
using MLAPI.Messaging;

public class SkillSystem : NetworkedBehaviour
{
	public List<BaseSkill> availableSkills = new List<BaseSkill>();

    public void Start(){
        //TBD something better
        BaseNetworkedEntity owner = GetComponent<BaseNetworkedEntity>(); 
        List<BaseSkill> local = new List<BaseSkill>();
        foreach(BaseSkill skill in availableSkills) {
            BaseSkill n = Instantiate(skill);
            n.owner = owner;
            local.Add(n);
        }
        availableSkills = local;
    }

    public BaseSkill GetSkillOfType(BaseSkill type){
        foreach(BaseSkill skill in availableSkills) {
            if(skill.name == type.name) return skill;
        }
        return null;
    }

    public void Update(){
    	foreach(BaseSkill skill in availableSkills) {
            if(skill != null) skill.Update();
    	}
    }

    public void UseSkill(int n){
    	if(!IsServer) return;
    	if(!availableSkills[n].usable) return;
    	availableSkills[n].Use();
    	InvokeClientRpcOnEveryone(UseSkillRPC, n);
    }

   	[ClientRPC]
	public void UseSkillRPC(int n){
		availableSkills[n].Use();
        Debug.Log("Used skill "+n);
	}
}
