﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LOSCamera : MonoBehaviour
{
    public RenderTexture targetTexture;
    public new Camera camera;
    public Material losMaterial;
    public Material blurMaterial;

    private RenderTexture blurTexture1;
    private RenderTexture blurTexture2;

    void Start()
    {
        targetTexture.width = Screen.width;
        targetTexture.height = Screen.height;

        camera.targetTexture = targetTexture;
        blurTexture1 = new RenderTexture(Screen.width, Screen.height, 16, RenderTextureFormat.R8);
        blurTexture2 = new RenderTexture(Screen.width, Screen.height, 16, RenderTextureFormat.R8);
        losMaterial.SetTexture("_Mask", blurTexture2);
    }

	void OnRenderImage(RenderTexture source, RenderTexture destination) {
        Graphics.Blit(targetTexture, blurTexture1, blurMaterial, 0);
        Graphics.Blit(blurTexture1, blurTexture2, blurMaterial, 1);
        Graphics.Blit(source, destination, losMaterial);
    }

}
