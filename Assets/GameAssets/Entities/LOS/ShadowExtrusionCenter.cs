﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowExtrusionCenter : MonoBehaviour
{
	public float extrusion;

    private void OnTriggerEnter(Collider other) {
        Renderer renderer = other.GetComponent<Renderer>();
        renderer.enabled = true;
        renderer.material.SetVector("_ExtrusionSource", transform.position);
        renderer.material.SetFloat("_TopExtrusion", extrusion);
    }

    private void OnTriggerStay(Collider other) {
        other.GetComponent<Renderer>()
            .material
            .SetVector("_ExtrusionSource", transform.position);
    }

    private void OnTriggerExit(Collider other) {
        other.GetComponent<Renderer>().enabled = false;
    }
}
