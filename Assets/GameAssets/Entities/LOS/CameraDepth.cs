﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDepth : MonoBehaviour
{
	public new Camera camera;
    void Start()
    {
        camera.depthTextureMode = DepthTextureMode.Depth;
    }
}
