﻿using UnityEngine;
using UnityEngine.Events;
using MLAPI;
using MLAPI.NetworkedVar;
using MLAPI.Messaging;

public class Interactable : NetworkedBehaviour
{
	public enum Type {Attack, Interact, Pickup};
	public Type type;
	public UnityEvent OnInteracted = new UnityEvent();

	public virtual void Interact(PlayerControllerScript interactor){
		OnInteracted.Invoke();
	}
}
