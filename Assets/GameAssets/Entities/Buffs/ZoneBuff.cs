﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneBuff : MonoBehaviour
{
    public BaseBuff buff;
    public bool onEnter = false;
    public bool onStay = false;
    public bool onExit = false;

    void OnCollisionEnter(Collision collision){
        if(onEnter) ApplyBuff(collision.gameObject);
    }

    void OnTriggerEnter(Collider collider){
        if(onEnter) ApplyBuff(collider.gameObject);
    }

	void OnCollisionStay(Collision collision){
		if(onStay) ApplyBuff(collision.gameObject);
    }

    void OnTriggerStay(Collider collider){
    	if(onStay) ApplyBuff(collider.gameObject);
    }

    void OnCollisionExit(Collision collision){
        if(onExit) ApplyBuff(collision.gameObject);
    }

    void OnTriggerExit(Collider collider){
        if(onExit) ApplyBuff(collider.gameObject);
    }

    void ApplyBuff(GameObject go){
    	if(go.TryGetComponent(out BuffSystem bs)){
    		bs.AddBuff(buff);
    	}
    }
}
