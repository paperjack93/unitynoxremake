﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Buffs/Base Buff")]
public class BaseBuff : ScriptableObject
{
	[HideInInspector] public int typeId;
	[HideInInspector] public int id;
	[HideInInspector] public bool isServer = false;
	[HideInInspector] public bool isClient = false;
	[HideInInspector] public bool isLocalPlayer = false;
	[HideInInspector] public BaseNetworkedEntity owner;
	[HideInInspector] public GameObject source;
	
	public new string name;
	public float duration;

	public bool stackable = false;

	[HideInInspector] public UnityEvent OnAdded = new UnityEvent();
	[HideInInspector] public UnityEvent OnUpdate = new UnityEvent();
	[HideInInspector] public UnityEvent OnExpired = new UnityEvent();

	public virtual void Added(){
		OnAdded.Invoke();
	}

	public virtual void UpdateBuff(float deltaTime){
		OnUpdate.Invoke();
		if(isServer){
			duration -= deltaTime;
			if(duration < 0) Expired();
		}
	}
	
	public virtual void Expired(){
		OnExpired.Invoke();
	}
}
