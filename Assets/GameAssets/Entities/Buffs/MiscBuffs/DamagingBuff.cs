﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Buffs/Damaging Buff")]
public class DamagingBuff : BaseBuff
{
	public Damage damagePerTick;
	public float tickDuration = 0.5f;
	float _tick = 0;

	public override void Added(){
		base.Added();
		damagePerTick.source = owner;
	}

	public override void UpdateBuff(float deltaTime){
		if(isServer){
			_tick -= deltaTime;
			while(_tick < 0) TickEnded();
		}
		base.UpdateBuff(deltaTime);
	}

	public virtual void TickEnded(){
		_tick += tickDuration;
		AttackSystem.ApplyDamage(owner.gameObject, damagePerTick);
	}
}
