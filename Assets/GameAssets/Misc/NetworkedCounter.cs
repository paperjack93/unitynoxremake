﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MLAPI;
using MLAPI.NetworkedVar;

public class NetworkedCounter : NetworkedBehaviour {
	public Utils.IntEvent OnIncreased;
	public Utils.IntEvent OnDecreased;
	public Utils.IntEvent OnChanged;
	public UnityEvent OnEmpty;
	public UnityEvent OnFull;

	public int maxAmount = 10;

	public int amount {
		get{ return _amount.Value;  }
		set{ 
			_amount.Value = Mathf.Clamp(value, 0, maxAmount);
		}
	}

	public bool empty {
		get{ return _amount.Value < 1;  }
	}

	public bool full {
		get{ return _amount.Value == maxAmount;  }
	}

	public NetworkedVarInt _amount = new NetworkedVarInt(new NetworkedVarSettings()
        {
            SendChannel = "us", 
            ReadPermission = NetworkedVarPermission.Everyone, 
            ReadPermissionCallback = null, 
            SendTickrate = 15, 
            WritePermission = NetworkedVarPermission.ServerOnly,
            WritePermissionCallback = null
        }
    );

	public void Awake(){
		_amount.OnValueChanged += AmountChanged;
	}

	void AmountChanged(int oldAmount, int newAmount){
		int delta = newAmount - oldAmount;
		if(newAmount > oldAmount) OnIncreased.Invoke(delta);
		if(newAmount < oldAmount) OnDecreased.Invoke(delta);
		if(newAmount != oldAmount) OnChanged.Invoke(delta);
		if(newAmount < 1 && oldAmount > 0) OnEmpty.Invoke();
		if(newAmount >= maxAmount && oldAmount < maxAmount) OnFull.Invoke();
    }
}
