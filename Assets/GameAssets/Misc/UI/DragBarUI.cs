﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragBarUI : EventTrigger {

    public RectTransform rt;
    Camera _camera;

    Rect _screenRect;
    Vector2 _size;
    Vector2 _offset;
    Canvas _canvas;
    bool _dragging;

    void OnEnable(){
        rt = transform.parent.GetComponent<RectTransform>();
        _canvas = GetComponentInParent<Canvas>();
        _camera = Camera.main;
        _size = (Vector2)_camera.ScreenToViewportPoint(rt.rect.size);
        Vector2 pos = new Vector2(0.5f-(_size.x/2), 0.5f-(_size.y/2));
        rt.anchorMin = pos;
        rt.anchorMax = pos;
    }

    public void LateUpdate() {
        if (_dragging) {
            Vector2 pos = (Vector2)_camera.ScreenToViewportPoint(Input.mousePosition) + _offset;

            if((pos.x+(_size.x*_canvas.transform.localScale.x)) > 1) pos.x = 1-(_size.x*_canvas.transform.localScale.x);
            else if(pos.x < 0) pos.x = 0;
            if((pos.y+(_size.y*_canvas.transform.localScale.y)) > 1) pos.y = 1-(_size.y*_canvas.transform.localScale.y);
            else if(pos.y < 0) pos.y = 0;

            rt.anchorMin = pos;
            rt.anchorMax = pos;
        }
    }

    public override void OnBeginDrag(PointerEventData eentData) {
        _offset = 
            (Vector2)_camera.ScreenToViewportPoint(
                new Vector2(transform.parent.position.x, transform.parent.position.y) 
                - new Vector2(Input.mousePosition.x, Input.mousePosition.y)
            );
        _dragging = true;
    }

    public override void OnEndDrag(PointerEventData eventData) {
        _dragging = false;
    }
}
