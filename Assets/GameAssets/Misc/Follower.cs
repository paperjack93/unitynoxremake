﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour
{
    public Transform target;
    public float positionLerp = 0;
    public float rotationLerp = 0;

    void LateUpdate(){
        if(!target) return;
        if(positionLerp == 0) transform.position = target.position;
        else if(positionLerp > 0) transform.position = Vector3.Lerp(transform.position, target.position, positionLerp * Time.deltaTime);
        if(rotationLerp == 0) transform.rotation = target.rotation;
        else if(rotationLerp > 0) transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, rotationLerp * Time.deltaTime);
    }
}
