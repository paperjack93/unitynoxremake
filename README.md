Unity Nox Remake

A somewhat faithful recreation of the old WestWood Nox game, made using a modern engine. 

Credits:

Paperjack (paperjack.itch.io)

DrainLive () - (3D models, 2D art)

CC0 wherever applicable
